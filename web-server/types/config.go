package types

type Config struct {
	Server struct {
		Port string `yaml:"port"`
		Host string `yaml:"host"`
	} `yaml:"server"`
	DB_Reader struct {
		Host     string `yaml:"host"`
		Username string `yaml:"user"`
		Password string `yaml:"pass"`
		DB       string `yaml:"db"`
		Port     string `yaml:"port"`
	} `yaml:"database_reader"`

	DB_Writer struct {
		Host     string `yaml:"host"`
		Username string `yaml:"user"`
		Password string `yaml:"pass"`
		DB       string `yaml:"db"`
		Port     string `yaml:"port"`
	} `yaml:"database_writer"`
}
