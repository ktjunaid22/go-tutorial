package types

type User struct {
	UserName   string `json:"username"`
	FullName   string `json:"full_name"`
	ProfileURL string `json:"profile_url"`
	Type       string `json:"type"`
	Gender     string `json:"gender"`
}
