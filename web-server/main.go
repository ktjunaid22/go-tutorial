package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	_ "github.com/lib/pq"
	"gopkg.in/yaml.v2"
)

var (
	db   *sql.DB
	dbWR *sql.DB
	cfg  types.Config
	ctx  context.Context
)

func main() {
	ctx = context.Background()

	loadConfig()
	initializeDB()
	initializeDBWR()

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	
	r.Use(cors.Handler)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("."))
	})

	userService := user.App{
		DBR:    db,
		DBW:    dbWR,
		Config: cfg,
		Ctx:    ctx,
	}
	r.Mount("/user", userService.Routes())

	http.DefaultClient.Timeout = time.Minute * 10
	log.Println("App started")
	log.Fatal(http.ListenAndServe(cfg.Server.Port, r))
}
// PostgreSQL
func initializeDB() {
	var err error
	dnsString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.DB_Reader.Username,
		cfg.DB_Reader.Password,
		cfg.DB_Reader.Host,
		cfg.DB_Reader.Port,
		cfg.DB_Reader.DB)
	db, err = sql.Open("postgres", dnsString)
	if err != nil {
		log.Fatal("db: ", err)
	}
	err = db.Ping()
	if err != nil {
		log.Fatal("db: ", err)
	}
}

func initializeDBWR() {
	var err error
	dnsString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.DB_Writer.Username,
		cfg.DB_Writer.Password,
		cfg.DB_Writer.Host,
		cfg.DB_Writer.Port,
		cfg.DB_Writer.DB)
	dbWR, err = sql.Open("postgres", dnsString)
	if err != nil {
		log.Fatal("db: ", err)
	}
	err = dbWR.Ping()
	if err != nil {
		log.Fatal("db: ", err)
	}
}

func loadConfig() {
	f, err := os.Open("config.yml")
	if err != nil {
		log.Fatal("config: ", err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		log.Fatal("config: ", err)
	}
}
