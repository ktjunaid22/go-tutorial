package errs

import "errors"

var (
	InternalServerError       = errors.New("Opps, something went wrong. Please try again.")
	BadRequest                = errors.New("Bad request.")
	Unauthorized              = errors.New("You are not authorized to perform this action.")
	InvalidHash               = errors.New("The encoded hash is not in the correct format.")
	NoHeader                  = errors.New("Authorization header required.")
	NoBearer                  = errors.New("Authorization requires Bearer scheme.")
	UnintendedExecution       = errors.New("This should not be executing.")
	PasswordNotMatch          = errors.New("Password does not match.")
	EmailorUsernameError      = errors.New("Provide valid email or username")
	EmailNotFound             = errors.New("Sorry, the email seems invalid. Looks like you have signed up using Facebook.")
	NoRowErr                  = errors.New("sql: no rows in result set")
	InvalidHashForPassword    = errors.New("Invalid hash found")
	ResetLinkused             = errors.New("Password reset link already used")
	ResetLinkExpired          = errors.New("Password reset link has expired")
)
